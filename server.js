var express = require('express');
 
var app = express();
 
app.get('/', function(req, res) {
res.sendfile(__dirname + '/pages/index.html');
});
app.get('/welcome/:name', function(req, res) {
res.send('Hello World ' + req.params.name + '!');
});
 
app.listen(8000);
console.log('Listening on port 8000...');
