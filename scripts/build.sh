SCRIPT_DIR=$(dirname "$0")
ROOT_DIR="$SCRIPT_DIR/.."
TARGET_DIR="$ROOT_DIR/target"

echo Installing modules
npm install

echo Preparing target folder
rm -rf  "$TARGET_DIR"
mkdir "$TARGET_DIR" ; mkdir -p "$TARGET_DIR/documentation"

echo Preparting distribution
cp -r *.js package.json node_modules pages "$TARGET_DIR"
cp "$ROOT_DIR/readme.txt" "$TARGET_DIR/documentation"

echo Done
